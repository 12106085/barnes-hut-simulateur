#include "../Include/AffichTree.h"

/*void InitWindow(GLFWwindow* window)
{
    if(!glfwInit())
    {
        printf("Erreur à l'initialisation de la librairie glfw\n");
        return;
    }

    window = glfwCreateWindow(DIMENSIONS, DIMENSIONS, "Simulateur Barnes-Hut", NULL, NULL);

    if (window != NULL) printf ("On a un problème\n");
    if (!window)
    {
        glfwTerminate();
        printf("Erreur survenue à la création de la fenêtre.\n");
        return;
    }

    glfwMakeContextCurrent(window);

    return;
}*/


// glOrtho permet de mettre les limites et l'échelle
// de l'espace de la fenêtre
//
// gluLookAt fonctionne comme décrit dans le git des physiciens
// c-a-d gluLookAt(camPosX,camPosY,camPosZ,lookAtX,lookAtY,lookAtZ,camDirX,camDirY,camDirZ)
void SetCamera(void)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-SCALE, SCALE, -SCALE, SCALE, -SCALE, SCALE);
    gluLookAt(10, 10, 20, 0, 0, 0, 0, 5, 0);
    glMatrixMode(GL_MODELVIEW);
}


// glBegin permet de définir quelle forme on va dessinner
// Pour les lignes cela fonctionne en définissant point de départ et d'arrivée
// mais il faut savoir que openGL garde le dernier point tracé comme point de départ
void DrawAxis(void)
{
    glColor3d(0.3, 0.3, 0.3);
    glBegin(GL_LINES);

    glVertex3d(-(SCALE/2), 0, 0);
    glVertex3d(SCALE/2, 0, 0);

    glVertex3d(0, -(SCALE/2), 0);
    glVertex3d(0, SCALE/2, 0);
    
    glVertex3d(0, 0, -(SCALE/2));
    glVertex3d(0, 0, SCALE/2);

    glEnd();
}



void AffichTreeRec(BHOctNoeud* arbre)
{
    if (arbre==NULL) return;
    
    if (arbre->isLeaf())        // Vérifie si le noeud a des enfants
    {
        
        glVertex3d(arbre->get_particule().position.x, arbre->get_particule().position.y, arbre->get_particule().position.z);
        
    }
    
    for (int i=0; i<8; i++) AffichTreeRec(arbre->get_fils(i));      // Appel récursif sur tous les enfants
                                                                    // pour parcourir tout l'arbre
    return;
}



void AffichTree(BHOctNoeud* arbre)
{
    glColor3d(0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);
 
    SetCamera();
    DrawAxis();
    glColor3d(0,0,1.0);
    
    glPointSize(7);
    glBegin(GL_POINTS);
    AffichTreeRec(arbre);
    glEnd();
    return;
}




























