#include "../Include/particule.h"
#include <random>
#include <cmath>
#include <ctime>
#include <chrono>




static const double masse_max = 5000.0;


/* Les constructeurs */


Particule::Particule(double x, double y, double z, const double m, double vx, double vy, double vz){
	position = sPOS{.x = x, .y = y, .z = z};
	masse = m;
	direction = sVEC{.vx = vx, .vy = vy, .vz = vz};
}


Particule::~Particule(){
}

/* Les get/set */

sPOS Particule::getPos(){ return position; }
void Particule::setPos(sPOS nPos){
	position = nPos;
}

double Particule::getMasse(){ return masse; }

sVEC Particule::getVec(){return direction;}
void Particule::setVec(sVEC nVec){
	direction = nVec;
}



// Change les coordonnées de la particule en fonction du vecteur 
void Particule::updatePos(){

	
	position.x += direction.vx + acceleration.ax/2;
	direction.vx += acceleration.ax;
	
	position.y += direction.vy + acceleration.ay/2;
	direction.vy += acceleration.ay;
	
	position.z += direction.vz + acceleration.az/2;
	direction.vz += acceleration.az;
	
	/*position.x = nextPosition.x;
	position.y = nextPosition.y;
	position.z = nextPosition.z;*/
	
	
}


















