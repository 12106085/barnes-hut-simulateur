#include "../Include/AffichTree.h"
#include <cstdlib>
#include <GL/freeglut_std.h>

    

GLFWwindow* window;

int main(void)
{

    if(!glfwInit())
    {
        printf("Erreur à l'initialisation de la librairie glfw\n");
        return -1;
    }

    window = glfwCreateWindow(DIMENSIONS, DIMENSIONS, "Simulateur Barnes-Hut", NULL, NULL);

    if (window == NULL)
    {
        glfwTerminate();
        printf("Erreur survenue à la création de la fenêtre.\n");
        return -1;
    }

    glfwMakeContextCurrent(window);

    SetCamera();

    BHOctNoeud racine (Vec3D(-taille_octree,-taille_octree,-taille_octree),Vec3D(taille_octree,taille_octree,taille_octree),NULL);
    
    srand (time(NULL));
    int x,y,z,vx,vy,vz;
    //racine.insert(Particule(0,20,0,10,1,0,0));
    //racine.insert(Particule(0,0,0,100,0,0,0));
    for(int i = 0; i<100; i++){
    	x = rand() % (taille_octree-100) - (taille_octree-100)/2;
    	y = rand() % (taille_octree-100) - (taille_octree-100)/2;
    	z = rand() % (taille_octree-100) - (taille_octree-100)/2;
    	Particule p = Particule(x,y,z,1,-y/20,z/20,-x/20);
    	racine.insert(p);
    	/*std::cout << "  " << "Particule " << i << ": ";
    	std::cout << "  " << "x=" << p.position.x << ";";
    	std::cout << "  " << "y=" << p.position.y << ";";
    	std::cout << "  " << "z=" << p.position.z << ")\n";*/
    	
    }
    printf("\n\n");
    racine.calculDistributionMasse();
    racine.Afficher_Donnees_Arbre(0,0);
    AffichTree(&racine);

    while (!glfwWindowShouldClose(window))
    {
	glfwPollEvents();
        
	racine.update();
	racine.Afficher_Donnees_Arbre(0,0);
	printf("\n\n");
	fflush(stdout);
	AffichTree(&racine);
        
	glfwWaitEventsTimeout(0.3f);
	glfwSwapBuffers(window);
        
    }

    return 1;
}
