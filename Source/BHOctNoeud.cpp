#include "../Include/BHOctNoeud.h"

//--- Standard includes --------------------------------------------------------
#include <cstdio>
#include <cstring>
#include <cassert>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <sstream>

//------------------------------------------------------------------------------
// Variables statiques
double BHOctNoeud::s_theta = 0.9;
double BHOctNoeud::s_gamma = 9.80665;
double BHOctNoeud::s_soft = 0.01;


BHOctNoeud::BHOctNoeud(const Vec3D &min,
                     const Vec3D &max,
                     BHOctNoeud *parent)
    :_parent(parent)
    ,_fils()
    ,_centre(min.x + (max.x - min.x) / 2.0,
             min.y + (max.y - min.y) / 2.0, 
             min.z + (max.z - min.z) / 2.0)
    ,_cm()
    ,_min(min)
    ,_max(max)
    ,_nbParticules(0)
    ,_masse_totale(0)
    ,_particule(0,0,0,0,0,0,0)
    
{
    for (int i=0 ; i<8 ; i++)
    	_fils[i] = nullptr;
    truc = 0;
}


bool BHOctNoeud::isRoot() const
{
    return _parent == nullptr;
}


bool BHOctNoeud::isLeaf() const
{
    bool res = false;
    for (int i=0 ; i<8 ; i++)
    	res = res || _fils[i];
    return !res;
}

const Vec3D &BHOctNoeud::get_cm() const
{
    return _cm;
}

const Vec3D &BHOctNoeud::get_centre() const
{
    return _centre;
}

const Vec3D &BHOctNoeud::get_min() const
{
    return _min;
}


const Vec3D &BHOctNoeud::get_max() const
{
    return _max;
}

/** Retourne le nombre de particules qui sont dans le noeud */
int BHOctNoeud::get_nbParticules() const
{
    return _nbParticules;
}

double BHOctNoeud::get_theta() const
{
    return s_theta;
}


void BHOctNoeud::set_theta(double theta)
{
    s_theta = theta;
}



void BHOctNoeud::reset(const Vec3D &min, const Vec3D &max)
{
    if (!isRoot())
        throw std::runtime_error("Seule la racine peut reset l'arbre");

    for (int i = 0; i < 8; ++i)
    {
        delete _fils[i];
        _fils[i] = nullptr;
    }

    _min = min;
    _max = max;
    _centre = Vec3D(min.x + (max.x - min.x) / 2.0,
                    min.y + (max.y - min.y) / 2.0,
                    min.z + (max.z - min.z) / 2.0);
    _nbParticules = 0;
    _masse_totale = 0;
    _cm = Vec3D(0, 0, 0);

}

// Retourne l'octant o� se trouve la coordonn�e mis en param�tre
BHOctNoeud::Octant BHOctNoeud::get_octant(double x, double y, double z) const
{
    
    if 	(x > _max.x || y > _max.y || z > _max.z
    	   ||	 x < _min.x || y < _min.y || z < _min.z)
    {
        std::stringstream ss;
        ss << "L'octant ne peut pas �tre d�termin�\n"
           << "particule  : "
           << "(" << x << ", " << y << ", " << z << ")\n"
           << "octMin   : "
           << "(" << _min.x << ", " << _min.y << ", " << _min.z << ")\n"
           << "octMax   : "
           << "(" << _max.x << ", " << _max.y << ", " << _max.z << ")\n"
           << "octcentre: "
           << "(" << _centre.x << ", " << _centre.y << ", " << _centre.z << ")\n";
        throw std::runtime_error(ss.str().c_str());
    }

    else if (x <= _centre.x && y <= _centre.y && z <= _centre.z) 	return NOI;
    else if (x <= _centre.x && y <= _centre.y && z >= _centre.z)	return NOE;
    else if (x <= _centre.x && y >= _centre.y && z <= _centre.z)	return SOI;
    else if (x <= _centre.x && y >= _centre.y && z >= _centre.z)	return SOE;
    else if (x >= _centre.x && y <= _centre.y && z <= _centre.z)	return NEI;
    else if (x >= _centre.x && y <= _centre.y && z >= _centre.z)	return NEE;
    else if (x >= _centre.x && y >= _centre.y && z <= _centre.z)	return SEI;
    else if (x >= _centre.x && y >= _centre.y && z >= _centre.z)	return SEE;
    
    
    else
        throw std::runtime_error("L'octant ne peut pas �tre d�termin�");
}

//Cr�ation d'un OctNode
BHOctNoeud *BHOctNoeud::createOctNode(Octant eOct)
{
    switch (eOct)
    {
    
    case NOI: 
    	return new BHOctNoeud(_min, _centre, this);

    case NOE : 
    	return new BHOctNoeud( Vec3D(_min.x, _min.y, _centre.z),
    			 	Vec3D(_centre.x, _centre.y, _max.z),
    				this);
    case NEE:
    	return new BHOctNoeud( Vec3D(_centre.x, _min.y, _centre.z),
    			 	Vec3D(_max.x, _centre.y, _max.z),
    				this);
    case NEI : 
    	return new BHOctNoeud( Vec3D(_centre.x, _min.y, _min.z),
    			 	Vec3D(_max.x,_centre.y,_centre.z),
    				this);
    case SOI : 
    	return new BHOctNoeud( Vec3D(_min.x, _centre.y, _min.z),
    			 	Vec3D(_centre.x, _max.y, _centre.z),
    				this);
    case SOE:
	return new BHOctNoeud(	Vec3D(_min.x, _centre.y, _centre.z),
    			 	Vec3D(_centre.x,_max.y,_max.z),
    				this);
    case SEE:
    	return new BHOctNoeud( _centre, _max, this);
	
    case SEI : 
    	return new BHOctNoeud( Vec3D(_centre.x, _centre.y, _min.z),
    			 	Vec3D(_max.x, _max.y, _centre.z),
    				this);
    
    default:
    {
        std::stringstream ss;
        ss << "L'octant ne peut pas �tre d�termin�\n";
        /*
                   << "particule  : " << "(" << x          << ", " << y          << ")\n"
                     << "octMin   : " << "(" << _min.x    << ", " << _min.y    << ")\n"
                     << "octMax   : " << "(" << _max.x    << ", " << _max.y    << ")\n"
                     << "octCentre: " << "(" << _centre.x << ", " << _centre.y << ")\n";
        */
        throw std::runtime_error(ss.str().c_str());
    }
    }
}

BHOctNoeud* BHOctNoeud::get_fils(int i){
	return _fils[i];
}

//Calcul le centre de masse de chaque noeud de l'arbre
void BHOctNoeud::calculDistributionMasse()
{
    for (int i=0;i<8;i++){
	    if(_fils[i] && _fils[i]->_nbParticules == 0){
			printf("nun\n");
			fflush(stdout);
			this->_fils[i] = nullptr;
	    }
    }
    
    if (_nbParticules == 1)
    {
        sPOS ps = _particule.position;
        _masse_totale = _particule.masse;
        _cm = Vec3D(ps.x, ps.y, ps.z);
    }
    else
    {
        _masse_totale = 0;
        _cm = Vec3D(0, 0, 0);

        for (int i = 0; i < 8; ++i)
        {
            if (_fils[i])
            {
                _fils[i]->calculDistributionMasse();
                _masse_totale += _fils[i]->_masse_totale;
                _cm.x += _fils[i]->_cm.x * _fils[i]->_masse_totale;
                _cm.y += _fils[i]->_cm.y * _fils[i]->_masse_totale;
                _cm.z += _fils[i]->_cm.z * _fils[i]->_masse_totale;
            }
        }

        _cm.x /= _masse_totale;
        _cm.y /= _masse_totale;
        _cm.z /= _masse_totale;
    }
}

//Affiche des donn�es de chaque noeud de l'arbre 
void BHOctNoeud::Afficher_Donnees_Arbre(int oct, int level)
{
    std::string space;
    for (int i = 0; i < level; ++i)
        space += "  ";

    std::cout << space << "Octant " << oct << ": ";
    std::cout << space << "(num=" << _nbParticules << "; ";
    std::cout << space << "masse=" << _masse_totale << ";";
    std::cout << space << "cm.x=" << _cm.x << ";";
    std::cout << space << "cm.y=" << _cm.y << ";";
    std::cout << space << "cm.z=" << _cm.z << ")\n";

    for (int i = 0; i < 8; ++i)
    {
        if (_fils[i])
        {
            _fils[i]->Afficher_Donnees_Arbre(i, level + 1);
        }
    }
}
	
//Ins�re une particule dans l'arbre
void BHOctNoeud::insert(const Particule &newParticle)
{
    
    //printf("Insert\n");
    //V�rifie que la particule est bien dans le bloc de l'arbre
    const sPOS p1(newParticle.position);
    if ((p1.x < _min.x || p1.x > _max.x) 
     || (p1.y < _min.y || p1.y > _max.y)
     || (p1.z < _min.z || p1.z > _max.z))
    {
        /*std::stringstream ss;
        ss << "La particule est en dehors de l'arbre\n"
           << "Position de la particule = " << "(" << p1.x << ", " << p1.y << ") "
           << "min.x =" << _min.x << ", "
           << "max.x =" << _max.x << ", "
           << "min.y =" << _min.y << ", "
           << "max.y =" << _max.y << ", "
           << "min.z =" << _min.z << ", "
           << "max.z =" << _max.z << ")";
        throw std::runtime_error(ss.str());*/
        return;
    }
    // S'il y a d�j� plus d'une particule dans le noeud : 
    // On peut le mettre dans un de ses fils (� cr�er s'il n'existe pas encore)
    if (_nbParticules > 1)
    {

        Octant eOct = get_octant(p1.x, p1.y, p1.z);
        if (!_fils[eOct])
            _fils[eOct] = createOctNode(eOct);

        _fils[eOct]->insert(newParticle);
        
    }
    // Le noeud contient une seule particule :
    // Il faut diviser ce bloc et relocaliser les deux particules
    else if (_nbParticules == 1)
    {
         //printf("num = 1 !!\n");
         fflush(stdout);
        assert(isLeaf() || isRoot());
        const sPOS p2(_particule.position);
        
        Octant eOct = get_octant(p2.x, p2.y, p2.z);
        if (_fils[eOct] == nullptr)
            _fils[eOct] = createOctNode(eOct);
        _fils[eOct]->insert(_particule);
        
        eOct = get_octant(p1.x, p1.y, p1.z);
        if (!_fils[eOct]){
            _fils[eOct] = createOctNode(eOct);
         }
        _fils[eOct]->insert(newParticle);
    }

    // Le noeud ne contient pas de particule :
    // Il suffit d'ajouter la particule
    else if (_nbParticules == 0)
    {
	
        fflush(stdout);
        _particule = newParticle;
        _masse_totale = _particule.masse;
    }

    _nbParticules++;
}

//calcul de la force entre chaque particule dans l'arbre

void BHOctNoeud::CalcForceAll(BHOctNoeud *racine){
	if(this == NULL) return;
	if (isLeaf()){
		_particule.acceleration.ax = 0;
		_particule.acceleration.ay = 0;
		_particule.acceleration.az = 0;
		Vec3D acc = racine->CalcTreeForce(_particule);
		printf("%g,%g,%g  \n", acc.x, acc.y, acc.z);
		_particule.acceleration.ax = acc.x;
		_particule.acceleration.ay = acc.y;
		_particule.acceleration.az = acc.z;
		//racine->rk4(&_particule);
		return;
	}
	for(int i=0; i<8; i++)
		_fils[i]->CalcForceAll(racine);
}

//Calcul de la force des particules de l'arbre sur une particule p1
Vec3D BHOctNoeud::CalcTreeForce(Particule &p1) const
{
    Vec3D acc;

    double r(0), k(0), d(0);
    if (_nbParticules == 1)
    {
        return CalcForceParticule(p1, _particule);
    }
    else
    {
        r = sqrt((p1.position.x - _cm.x) * (p1.position.x - _cm.x) +
                 (p1.position.y - _cm.y) * (p1.position.y - _cm.y) +
                 (p1.position.z - _cm.z) * (p1.position.z - _cm.z) + s_soft);
        d = _max.x - _min.x;
        
        if (d / r <= s_theta)
        {
            k = s_gamma * _masse_totale / (r * r * r);
            acc.x = k * (_cm.x - p1.position.x);
            acc.y = k * (_cm.y - p1.position.y);
            acc.z = k * (_cm.z - p1.position.z);
        }
        else
        {
            Vec3D buf;
            for (int q = 0; q < 8; ++q)
            {
                if (_fils[q])
                {
                    buf = _fils[q]->CalcTreeForce(p1);
                    acc.x += buf.x;
                    acc.y += buf.y;
                    acc.z += buf.z;
                } 
            }     
        }
    }

    return acc;
}

//Calcul de la force par une particule p2 sur une particule p1
Vec3D BHOctNoeud::CalcForceParticule(const Particule &p1, const Particule &p2) const
{
    Vec3D acc;

    if (&p1 == &p2){
        return acc;
    }

    const double &x1(p1.position.x),
                 &y1(p1.position.y),
                 &z1(p1.position.z);
    const double &x2(p2.position.x),
                 &y2(p2.position.y),
                 &z2(p2.position.z),
                 &m2(p2.masse);

    double r = sqrt((x1 - x2) * (x1 - x2) +
                    (y1 - y2) * (y1 - y2) + 
                    (z1 - z2) * (z1 - z2) + s_soft);

    double k = s_gamma * m2 / (r * r * r);

    
    if (r > 0.001)
    {
        double k = s_gamma * m2 / (r * r * r);

        acc.x = k * (x2 - x1);
        acc.y = k * (y2 - y1);
        acc.z = k * (z2 - z1);
    }
    else
        acc.x = acc.y = acc.z = 0;

    return acc;
}

Particule BHOctNoeud::get_particule(){
    return _particule;
}

//Met � jour la position de chaque particule (on ne modifie pas encore son emplacement dans l'arbre)
void BHOctNoeud::updatePosAll(){
	if(this == NULL) return;
	if (isLeaf()){
		_particule.updatePos();
		updateOctantParticule(_particule); 
		return;
	}
	for(int i=0; i<8; i++)
		_fils[i]->updatePosAll();
		
}

// calcul de force et maj de l'arbre
void BHOctNoeud::update(){
    sVEC acc[50];
    //On calcule la force de chaque particule
    CalcForceAll(this);
    //On met � jour la position des particules
    updatePosAll();

    //On recalcule la distribution de masse de chaque noeud
    calculDistributionMasse();
    
    
    
}





void BHOctNoeud::updateOctantParticule(Particule p){
   
    
    double x = p.position.x, y = p.position.y, z = p.position.z;
    
    //bon_oct nous dit si la particule est bien dans le bon noeud
    bool bon_oct = !(x > _max.x || y > _max.y || z > _max.z
    	   ||	 x < _min.x || y < _min.y || z < _min.z);
    	   
    //Si la particule est dans le bon octant et est bien plac�, il n'y a rien � faire
    if (isLeaf() && bon_oct){
	return;
    }
    
    //On retire les donn�es de la particule dans le noeud courant 
    _nbParticules--;
    _masse_totale -= p.masse;
    
    //On enl�ve les noeuds qui sont vides
    for (int i=0;i<8;i++){
	    if(_fils[i] != nullptr && _fils[i]->_nbParticules == 0){
			this->_fils[i] = nullptr;
	    }
    }
    //Si le noeud a un seul enfant qui est une feuille, on retire son enfant et on lui donne sa particule
    	if (_nbParticules == 1 && !isLeaf()){
		for (int i=0;i<8;i++){
		    if(_fils[i] != nullptr && _fils[i]->_nbParticules == 1){
				_particule = _fils[i]->_particule;
				_fils[i] = nullptr;
	    		}
   		}
   	}
   	
   	
    // Si la particule n'est pas plac� dans le bon octant, on le supprime du noeud courant (pas de l'arborescence attention)
    if(!bon_oct){
	// Si le noeud n'est pas la racine, on appelle r�cursivement la fonction afin de remonter jusqu'� ce qu'on trouve une partie de l'arbre o� la position de la particule est bonne
	if(!isRoot()){
		_parent->updateOctantParticule(p);
		return;
	}
	//Sinon on fait rien, on laisse la particule tranquille
	printf("Bye bye particule...");
	std::cout << "  " << "x=" << p.position.x << ";";
    	std::cout << "  " << "y=" << p.position.y << ";";
    	std::cout << "  " << "z=" << p.position.z << ")\n";
	return;
    }
    //Si on arrive l� c'est qu'on est dans le bon octant et que le noeud n'est pas une racine.

    //Dans ce cas, on insert la particule � partir d'o� on se trouve (sachant qu'on n'a logiquement pas touch� les parents du noeud courant, on n'a rien � faire en haut)
    
    insert(p); 
    
}


void BHOctNoeud::rk4( Particule *p){
	sVEC k1,k2,k3,k4;
	
	k1 = eval(p);
	
	p->direction.vx += 0.5*k1.vx;
	p->direction.vy += 0.5*k1.vy;
	p->direction.vz += 0.5*k1.vz;
	
    	k2 = eval(p);
    	p->direction.vx -= 0.5*k1.vx;
	p->direction.vy -= 0.5*k1.vy;
	p->direction.vz -= 0.5*k1.vz;
	
	p->direction.vx += 0.5*k2.vx;
	p->direction.vy += 0.5*k2.vy;
	p->direction.vz += 0.5*k2.vz;
    	k3 = eval(p);

	p->direction.vx -= 0.5*k2.vx;
	p->direction.vy -= 0.5*k2.vy;
	p->direction.vz -= 0.5*k2.vz;
	
	p->direction.vx += k3.vx;
	p->direction.vy += k3.vy;
	p->direction.vz += k3.vz;
	
    	k4 = eval(p);
    	
    	p->direction.vx -= k3.vx;
	p->direction.vy -= k3.vy;
	p->direction.vz -= k3.vz;
    	
    	p->nextPosition.x += (1/6.)*(k1.vx+ 2*k2.vx + 2*k3.vx + k4.vx);
    	p->nextPosition.y += (1/6.)*(k1.vy+ 2*k2.vy + 2*k3.vy + k4.vy);
    	p->nextPosition.z += (1/6.)*(k1.vz+ 2*k2.vz + 2*k3.vz + k4.vz);
    	printf("next position x = %g\n\n", p->nextPosition.x);
    	
}    	
    
sVEC BHOctNoeud::eval( Particule *p){
	Vec3D acc =CalcTreeForce(*p);
	sVEC vitesse;
	
	vitesse.vx += acc.x;
	vitesse.vy += acc.y;
	vitesse.vz += acc.z;
	
	return  vitesse;
}


BHOctNoeud::~BHOctNoeud()
{
    for (int i = 0; i < 8; ++i)
        delete _fils[i];
}
