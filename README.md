# Barnes-Hut Simulateur

Ce fichier est modifiable par tous les auteurs et contributeurs de ce projet.

## Description
Ce projet a pour but de créer un simulateur, de collision, de particules dans l'espace. Une simulation des forces gravitationnelles, de chaque particules, est aussi présente.

## Description technique
Pour ce programme la structure choisi, afin de représenter les objets célestes, a été un arbre de données. Pour représenter l'espace en 3 dimensions cet arbre est donc un oct-arbre. Chaque fils d'un noeud représente un espace tridimensionnelle de ce noeud qui représente un espace fini. 

## Visuals
Pour une représentation graphique de ce simulateur, 3 axes sont tracés dans une fenêtre et des points représentant les particules se déplacent. Cette affichage est implémenté à l'aide des bibliothèques de OpenGL.

## Installation
Des prérequis sont tout de même attendu pour exécuter ce programme. Ce programme a été développé pour être exécutable sur le système d'exploitation Linux. Veuillez aussi installer les paquets suivants : libegl-mesa0; libegl1-mesa-dev; libgl1-mesa-dev; libgl1-mesa-dri; libglapi-mesa; libglu1-mesa; libglu1-mesa-dev; libglx-mesa0; mesa-common-dev; mesa-utils; mesa-utils-bin; mesa-vulkan-drivers

## Usage
Ce projet est à but d'évaluation universitaire et n'est relié à aucune entreprise privée. Il peut être réutilisé par ses auteurs, afin de l'utiliser comme démonstration de leurs compétences.


## Authors and acknowledgment
Lydia Bakiri
Mounir Abi Ayad
Sasha

## License
Ouvert à toutes utilisations.

## Project status
Le programme s'exécute correctement et cette version est stable. Elle comprend les fonctionnalités suivantes :
        - Oct-arbre implémenté et s'actualisant au fil du temps
        - Calcul de force (niveau préliminaires) sur toutes les particules
        - Affichage de la structure en temps réel

Il nous reste tout de même deux axes d'améliorations. Le premier étant une plus grande précision sur le calcul des forces gravitationnelles de chaque particules. Et le deuxième étant la gestion des collisions, des objets entre eux.
