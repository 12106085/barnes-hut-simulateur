#ifndef BHOCTNOEUD_H
#define BHOCTNOEUD_H

#include <vector>
#include <array>

#include "Vector.h"
#include "particule.h"

constexpr unsigned max_fils = 8;


// Classe d'un noeud d'un arbre de Barnes Hut
class BHOctNoeud {

private :

	
	BHOctNoeud* _parent;		//parent du noeud (null si racine)
	std::array<BHOctNoeud*, max_fils> _fils;	//fils du noeud (entre 0 et 8)
	Vec3D _centre;             	// Centre du noeud
	Vec3D _cm;                 	// Centre de masse du noeud
    	Vec3D _min;                	// Coin bas gauche arrière du noeud
    	Vec3D _max;                	// Coin haut droite avant du noeud
	int _nbParticules;
	double _masse_totale;		// Masse de l'ensEmble des particules dans le noeud
	Particule _particule; 	//Dans le cas o� le noeud est une feuille
	
	
	//Valeurs partag�es entre tous les noeuds de l'arbre
	static double s_gamma; 	// constante gravitationelle
	static double s_soft;
	
	// � utiliser pour le MAC 
	static double s_theta;
	bool tropProche;		// Vrai si le noeud ne permet pas l'approximation
	
	
	//M�thodes pour le calcul de force
	Vec3D CalcForce(const Particule &p1) const;
	Vec3D CalcForceParticule(const Particule &p1, const Particule &p2) const;
    	Vec3D CalcTreeForce(Particule &p1) const;
    	void CalcForceAll(BHOctNoeud *racine);
	
	

public :

	// Enum�ration des octants
	enum Octant {
		NOI = 0,
		NEI,
		SOI,
		SEI,
		NOE,
		NEE,
		SOE,
		SEE,
		NONE
	};
	int truc;
	
	
	//Constructeur
	
	BHOctNoeud(const Vec3D &min,
	           const Vec3D &max,
	           BHOctNoeud *parent = nullptr);
	~BHOctNoeud();

	//Getters
	BHOctNoeud *get_fils(int i);
	/*BHOctNoeud *get_parent();*/
	Particule get_particule();
	
	const Vec3D &get_cm() const;
	const Vec3D &get_centre() const;
	const Vec3D &get_min() const;
    	const Vec3D &get_max() const;
	int get_nbParticules() const;
	double get_theta() const;
	double get_gamma();
	Octant get_octant(double x, double y, double z) const;
	

	//Setters
	void set_cm(const Vec3D &);
	void set_nbParticule(unsigned _nbParticule);
	void set_theta(double theta);
	
	//Insertion d'une nouvelle particule
	void insert(const Particule &newParticle);
	
	
	//Creation d'un fils dont la coordonn�e en param�tre en fait partie
	BHOctNoeud *createOctNode(Octant eOct);
	
	
	//Reset : Remet � 0 cette partie du noeud
	void reset(const Vec3D &min,
	           const Vec3D &max);
	
	bool isRoot() const;
	bool isLeaf() const;
	
	void calculDistributionMasse();
	void Afficher_Donnees_Arbre(int oct, int level);
	
	void updateOctant();
	void updatePosAll();
	void updateOctantParticule(Particule p);
	void update();
	
	void rk4(Particule *p);
	sVEC eval(Particule *p);
};



#endif
