#define DIMENSIONS 600
#define SCALE 100
#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include "BHOctNoeud.h"

// Ne sera pas utilisé, car je n'arrive pas à init
// la fenêtre à travers une fonction externe au main
void InitWindow(GLFWwindow* window);
void SetCamera(void);
void DrawAxis(void);
//  fonction d'initialisation à la fonction récursive
void AffichTree(BHOctNoeud* arbre);
void AffichTreeRec(BHOctNoeud* arbre);
