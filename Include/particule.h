#ifndef PARTICULE
#define PARTICULE
#define SCALE 100


static const int taille_octree = 200; // la taille de la "boundbox" 
struct sPOS{
	double x;
	double y;
	double z;
};

struct sVEC{
	double vx;
	double vy;
	double vz;
};

struct sACC{
	double ax;
	double ay;
	double az;
};

class Particule{

	public:
		// Constructeurs
		Particule(double x, double y, double z, double m, double vx, double vy, double vz);
		~Particule();
		
		// Get/Set
		sPOS getPos();
		void setPos(sPOS nPos);
		
		double getMasse();
		
		sVEC getVec();
		void setVec(sVEC nVec);
		
		
		// Change les vecteurs en fonction de l'attraction entre la particule et le centre de masse/particule
		sVEC attraction(sPOS pCoord, double nMasse);
		
		
		// Change la position de la particule
		void updatePos();
		
		
		
	//private:
	
		sPOS position;
		sPOS nextPosition;
		double masse;
		sVEC direction;
		sACC acceleration;
		
		
};

#endif

